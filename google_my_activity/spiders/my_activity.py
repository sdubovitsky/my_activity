import datetime
import json
import re
import time
import urllib

from http.cookies import SimpleCookie

import scrapy
import requests


class MyActivitySpider(scrapy.Spider):
    name = 'my_activity'
    start_urls = ['https://myactivity.google.com/myactivity?product=26']
    user_cookies = None
    headers = {
        'X-Same-Domain': '1',
        'Sec-Fetch-Dest': 'empty',
        'User-Agent': ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) '
                       'AppleWebKit/537.36 (KHTML, like Gecko) '
                       'Chrome/80.0.3987.149 Safari/537.36'),
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        'Accept': '*/*',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors'
    }
    batch_execute_url = ('https://myactivity.google.com/'
                         '_/FootprintsMyactivityUi/data/batchexecute')

    def start_requests(self):
        raw_cookies = self.user_cookies.split('cookies: ')[1]
        cookie = SimpleCookie()
        cookie.load(raw_cookies)
        self.user_cookies = {k: v.value for k, v in cookie.items()}
        for url in self.start_urls:
            yield scrapy.http.Request(
                url,
                cookies=self.user_cookies,
                headers=self.headers,
            )

    @staticmethod
    def _get_payload(response) -> str:
        at_pattern = re.compile(r"(\w+:\d+)")
        at_value = response.xpath('//script[@id="_ij"]').re(at_pattern)[0]

        f_req_pattern = re.compile(r";(\w.+);")
        f_req_value = response.xpath('//c-data[@id="i9"]').re(f_req_pattern)[0]

        return urllib.parse.urlencode({
            'at': at_value,
            'f.req': [[[
                "QmiuMd",
                f"[\"{f_req_value}\",null,[[],null,null,null,[[26]]]]",
                'null', "generic"
            ]]]
        })

    @staticmethod
    def _get_params(response):
        f_sid_pattern = re.compile(r'":\"(\d{10,})\"')
        f_sid = response.xpath('//script[@data-id="_gd"]').re(f_sid_pattern)[0]
        return {
            'rpcids': 'QmiuMd',
            'f.sid': f_sid,
        }

    @staticmethod
    def _get_af_data(response):
        pattern = re.compile(r'{return (.*)}}\);</script>', re.DOTALL)
        raw_array = response.xpath('//script')[-3].re(pattern)[0]
        data_list = (json.loads(raw_array
                                .replace('\\n', "\n")
                                .replace('\\"', "\"")))
        return data_list

    @staticmethod
    def _get_action_data(action):
        action_data = action[9]
        action_datetime = (datetime
                           .datetime
                           .fromtimestamp(action[4] / 10 ** 6))
        return {
            'datetime': action_datetime.isoformat(),
            'title': action_data[0],
            'link': action_data[3],
        }

    def parse(self, response):
        self.logger.debug(f'Response: {response}')
        payload = self._get_payload(response)
        params = self._get_params(response)
        af_data_list = self._get_af_data(response)
        actions = []
        try:
            today_action_list = af_data_list[0][0][2]
            for a in today_action_list:
                actions.append(self._get_action_data(a))
        except IndexError:
            pass

        resp_kwargs = dict(
            cookies=self.user_cookies,
            headers=self.headers,
            params=params,
            data=payload,
        )

        more_action_resp = requests.post(
            self.batch_execute_url,
            **resp_kwargs
        )

        # sometimes the first request returns an error
        # Todo research
        if more_action_resp.status_code != 200:
            self.logger.warning('Retry request for more action')
            time.sleep(2)
            more_action_resp = requests.post(
                self.batch_execute_url,
                **resp_kwargs
            )

        self.logger.debug(f'More actions response: {more_action_resp.text}')
        regex = r"\"QmiuMd\",\"(.+)\","

        actions_array = []
        raw_actions = re.findall(regex, more_action_resp.text)
        actions_string = raw_actions[0] if raw_actions else None
        if actions_string:
            actions_array = json.loads(actions_string
                                       .replace('\\n', "\n")
                                       .replace('\\"', "\""))[0]
        if actions_array:
            actions = []
            for a in actions_array:
                actions.append(self._get_action_data(a))
            self.logger.debug(f'Results: {actions}')
        return actions

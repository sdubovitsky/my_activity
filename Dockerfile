FROM python:3.7.0-slim
ENV TERM xterm
ENV SCRAPY_SETTINGS_MODULE google_my_activity.settings
RUN mkdir -p /app
WORKDIR /app
COPY ./requirements.txt /app/requirements.txt
RUN pip install -U pip
RUN pip install --no-cache-dir -r requirements.txt
COPY . /app
RUN python setup.py install

import unittest

from unittest import mock

from google_my_activity.spiders.my_activity import MyActivitySpider
from .utils import get_fake_response


class MyActivitySpiderTest(unittest.TestCase):

    @mock.patch('google_my_activity.spiders.my_activity.requests.post')
    def test_parser(self, post_mock):
        post_mock.return_value = get_fake_response(
            'batch_execute_resp.txt',
            response_class_name='dummy'
        )
        spider = MyActivitySpider()
        cards = spider.parse(get_fake_response('my_activity_html.html'))
        self.assertEqual(len(cards), 9)

    @mock.patch('google_my_activity.spiders.my_activity.requests.post')
    def test_without_more_action(self, post_mock):
        post_mock.return_value = get_fake_response(
            'batch_execute_resp_without_more_action.txt',
            response_class_name='dummy',
            status=400
        )
        spider = MyActivitySpider()
        cards = spider.parse(
            get_fake_response('my_activity_html_without_more_action.html'))
        self.assertEqual(len(cards), 2)

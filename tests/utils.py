from scrapy.http import HtmlResponse, Request


class DummyResponse:

    def __init__(self,
                 status: int = 200,
                 content: bytes = b'',
                 ):
        self.status_code = status
        self.status = status
        self.content = content
        self.text = content.decode()


def get_fake_response(
        file_name: str,
        url: str = 'http://www.example.com',
        response_class_name: str = 'html',
        status: int = 200,
):

    with open(f'tests/data/{file_name}', 'rb') as f:
        content = f.read()

    response = HtmlResponse(
        url=url,
        request=Request(url=url),
        body=content,
        status=status
    ) if response_class_name == 'html' else \
        DummyResponse(content=content,
                      status=status)

    return response

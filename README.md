```
pip install shub
shub login
API key: <API_KEY>
shub deploy <APP>
```

If deploy with errors:
https://support.scrapinghub.com/support/solutions/articles/22000232799-errors-while-deploying-custom-image-to-scrapy-cloud


Run crawler
```
scrapy crawl my_activity -a user_cookies=<USER_GOOGLE_COOKIES>
```

Run unittests:
```
python -m unittest
```
